/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DirectorioT.bo;

import DirectorioT.Entities.Tramite;
import DirectorioT.dao.TramiteDAO;
import java.util.LinkedList;

/**
 *
 * @author Allan Murillo
 */
public class TramiteBO {
    //LinkedList donde se cargan los tramites
    public LinkedList<Tramite> cargarTramites(String filtro, char estado) {
        return new TramiteDAO().cargar(filtro.trim(), estado);
    }

    //Metodo no funcional
    public void eliminar(int id) {
        if (id > 0) {
            new TramiteDAO().activar(id, false);
        } else {
            throw new RuntimeException("Favor seleccione un artículo");
        }
    }
    //Metodo no funcional
    public void activar(int id) {
        if (id > 0) {
            new TramiteDAO().activar(id, true);
        } else {
            throw new RuntimeException("Favor seleccione un artículo");
        }
    }
    //Validacion de los datos de trámite
    private void validarTramite(Tramite ent) {
        if (ent == null) {
            throw new RuntimeException("Datos inválidos!!");
        }
        if (ent.getCodigo().isEmpty()) {
            throw new RuntimeException("Codigo requerido");
        }

        if (ent.getDescripcion().isBlank()) {
            throw new RuntimeException("La descripcion es requerida");
        }

        if (ent.getRequisitos().isBlank()) {
            throw new RuntimeException("Los requisitos son requeridos");
        }
        
        if (ent.getFecha_creacion()==null){
            throw new RuntimeException("Fecha de creación requerida");
        }
        
        if (ent.getFecha_modificacion()==null){
            throw new RuntimeException("Fecha de modificacion requerida");
        }
    }
    //Donde el id del Trámite sea 0 guardará el trámite como uno nuevo
    public void guardarTramite(Tramite art) {
        validarTramite(art);
        if(art.getId()== 0){
            new TramiteDAO().insertarTramite(art);
        }
    }

}
