/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DirectorioT.dao;

import DirectorioT.Entities.Tramite;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import javax.swing.JOptionPane;
import java.sql.Date;

/**
 *
 * @author Allan Murillo
 */
public class TramiteDAO {
//Se hace la insercion en la base de datos de el trámite
    public void insertarTramite(Tramite t) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "INSERT INTO app.tramite (codigo, titulo, descripcion, requisitos, precio, "
                    + "fecha_creacion, fecha_modificacion, estado) "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, t.getCodigo());
            stmt.setString(2, t.getTitulo());
            stmt.setString(3, t.getDescripcion());
            stmt.setString(4, t.getRequisitos());
            stmt.setDouble(5, t.getPrecio());
            stmt.setDate(6, new java.sql.Date(t.getFecha_creacion().getTime()) );
            stmt.setDate(7, new java.sql.Date(t.getFecha_modificacion().getTime()));
            stmt.setBoolean(8, t.isEstado());
            stmt.executeUpdate();
            JOptionPane.showMessageDialog(null, "Trámite registrado");
        } catch (Exception e) {
            throw new RuntimeException("Problemas de conexión con el servidor 1");
        }

    }
    //Aquí se cargan los tramites que posteriormente irian a la tabla de modificar
    public LinkedList<Tramite> cargar(String filtro, char estado) {
        LinkedList<Tramite> tramites = new LinkedList<>();
        filtro = generarFiltro(filtro, estado);
        try ( Connection con = Conexion.getConexion()) {
            String sql = "SELECT id, codigo, titulo, descripcion, requisitos, precio, fecha_creacion, fecha_modificacion, estado FROM app.tramite";
            sql += filtro;
            PreparedStatement stmt = con.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                tramites.add(cargarTramite(rs));
            }
            stmt.clearParameters();
            return tramites;      
        } catch (Exception e) {
            throw new RuntimeException("Problemas de conexión con el servidor 1");   
        } 
    }
    
    //Se carga todos los datos necesarios para mostrar
    private Tramite cargarTramite(ResultSet rs) throws SQLException {
        Tramite tr = new Tramite();
        tr.setId(rs.getInt("id"));
        tr.setCodigo(rs.getString("codigo"));
        tr.setTitulo(rs.getString("titulo"));
        tr.setDescripcion(rs.getString("descripcion"));
        tr.setRequisitos(rs.getString("requisitos"));
        tr.setPrecio(rs.getInt("precio"));
        tr.setFecha_creacion(Date.valueOf(rs.getString("fecha_creacion")));
        tr.setFecha_modificacion(Date.valueOf(rs.getString("fecha_modificacion")));
        tr.setEstado(rs.getBoolean("estado"));
        cargar(tr, rs);
        return tr;
    }

    private void cargar(Tramite tr, ResultSet rs) throws SQLException {
        tr.setId(rs.getInt("id"));
        tr.setCodigo(rs.getString("codigo"));
        tr.setTitulo(rs.getString("titulo"));
        tr.setDescripcion(rs.getString("descripcion"));
        tr.setRequisitos(rs.getString("requisitos"));
        tr.setPrecio(rs.getDouble("precio"));
        tr.setFecha_creacion(Date.valueOf(rs.getString("fecha_creacion")));
        tr.setFecha_modificacion(Date.valueOf(rs.getString("fecha_modificacion")));
        tr.setEstado(rs.getBoolean("estado"));
    }
    
    //Función no funcional
    public void activar(int id, boolean activo) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "update app.tramites set estado = ? where id = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setBoolean(1, activo);
            stmt.setInt(2, id);
            stmt.executeUpdate();
        } catch (Exception e) {
            throw new RuntimeException("Problemas de conexión con el servidor");
        }
    }

    private String generarFiltro(String filtro, char estado) {
        String txt = "";
        if (filtro.isBlank()) {
            if (estado != 'T') {
                txt = String.format(" where estado = %s", (estado == 'A' ? "true" : "false"));
            }
        } else if (!filtro.isBlank()) {
            txt = " where (lower(codigo) like lower('%s') or  lower(nombre) like lower('%s') or  lower(descripcion) like lower('%s'))";
            String dato = "%" + filtro.trim() + "%";
            txt = String.format(txt, dato, dato, dato);
            if (estado != 'T') {
                txt += String.format(" and (estado = %s)", (estado == 'A' ? "true" : "false"));
            }
        }
        return txt;
    }
}
