/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DirectorioT.dao;

import DirectorioT.Entities.Cliente;
import DirectorioT.Entities.Entidad;
import DirectorioT.Entities.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author Allan Murillo
 */
public class UsuarioDAO {

    public boolean insertarCliente(Cliente usu) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "INSERT INTO app.cliente (usuario, contra,"
                    + "tipo, nombre, cedula, direccion, correo, telefono,"
                    + " latitud, longitud) "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, usu.getUsuario());
            stmt.setString(2, usu.getContrasenna());
            stmt.setString(3, usu.getTipo());
            stmt.setString(4, usu.getNombre());
            stmt.setString(5, usu.getCedula());
            stmt.setString(6, usu.getDireccion());
            stmt.setString(7, usu.getCorreo());
            stmt.setString(8, usu.getTelefono());
            stmt.setString(9, usu.getLatitud());
            stmt.setString(10, usu.getLongitud());
            System.out.println(sql);
            return stmt.executeUpdate() > 0;
        } catch (Exception e) {
            throw new RuntimeException("Dato ya existe!!");
        }
    }

    public boolean insertarEntidad(Entidad ent) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "INSERT INTO app.entidad (usuario, contra,"
                    + "tipo, cedula_juridica, nombre, direccion, correo, telefono,"
                    + "latitud, longitud) "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, ent.getUsuario());
            stmt.setString(2, ent.getContrasenna());
            stmt.setString(3, ent.getTipo());
            stmt.setString(4, ent.getCedula_juridica());
            stmt.setString(5, ent.getNombre());
            stmt.setString(6, ent.getDireccion());
            stmt.setString(7, ent.getCorreo());
            stmt.setString(8, ent.getTelefono());
            stmt.setString(9, ent.getLatitud());
            stmt.setString(10, ent.getLongitud());
            return stmt.executeUpdate() > 0;
        } catch (Exception e) {
            throw new RuntimeException("Dato ya existe!!");
        }
    }

    public Cliente loginCliente(Cliente usu) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "SELECT  usuario, contra, tipo,"
                    + "id, nombre, cedula, direccion, correo,"
                    + "telefono, latitud, longitud FROM app.cliente "
                    + "WHERE contra = ? and (usuario = ? or correo = ?)";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, usu.getContrasenna());
            stmt.setString(2, usu.getUsuario());
            stmt.setString(3, usu.getCorreo());
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return cargarCliente(rs);
            }
            return usu;
        } catch (Exception e) {
            throw new RuntimeException("Problemas de conexión con el servidor");
        }
    }

    public Entidad loginEntidad(Entidad ent) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "SELECT  usuario, contra, tipo,"
                    + "id, cedula_juridica, nombre, direccion, direccion, correo,"
                    + "telefono, latitud, longitud, aprobada FROM app.entidad "
                    + "WHERE contra = ? and (usuario = ? or correo = ?)";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, ent.getContrasenna());
            stmt.setString(2, ent.getUsuario());
            stmt.setString(3, ent.getCorreo());
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return cargarEntidad(rs);
            }
            return ent;
        } catch (Exception e) {
            throw new RuntimeException("Problemas de conexión con el servidor");
        }
    }

    public Usuario login(Usuario usu) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "SELECT usuario, contra, tipo FROM app.usuarios "
                    + "WHERE contra = ? and usuario = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, usu.getContrasenna());
            stmt.setString(2, usu.getUsuario());
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return cargar(rs);
            }
            return usu;
        } catch (Exception e) {
            throw new RuntimeException("Problemas de conexión con el servidor");
        }
    }

    private Cliente cargarCliente(ResultSet rs) throws SQLException {
        Cliente u = new Cliente();
        u.setUsuario(rs.getString(1));
        u.setContrasenna(rs.getString(2));
        u.setTipo(rs.getString(3));
        u.setId(rs.getInt(4));
        u.setNombre(rs.getString(5));
        u.setCedula(rs.getString(6));
        u.setDireccion(rs.getString(7));
        u.setCorreo(rs.getString(8));
        u.setTelefono(rs.getString(9));
        u.setLatitud(rs.getString(10));
        u.setLongitud(rs.getString(11));
        return u;
    }

    private Entidad cargarEntidad(ResultSet rs) throws SQLException {
        Entidad u = new Entidad();
        u.setUsuario(rs.getString(1));
        u.setContrasenna(rs.getString(2));
        u.setTipo(rs.getString(3));
        u.setId(rs.getInt(4));
        u.setCedula_juridica(rs.getString(6));
        u.setNombre(rs.getString(5));
        u.setDireccion(rs.getString(7));
        u.setCorreo(rs.getString(8));
        u.setTelefono(rs.getString(9));
        u.setLatitud(rs.getString(10));
        u.setLongitud(rs.getString(11));
        return u;
    }

    private Usuario cargar(ResultSet rs) throws SQLException {
        Usuario u = new Usuario();
        u.setUsuario(rs.getString(1));
        u.setContrasenna(rs.getString(2));
        u.setTipo(rs.getString(3));
        return u;
    }
    
    //Función no funcional
    public boolean actualizarCliente(Cliente cl) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "update app.cliente set usuario=?, contra=?, tipo=?,"
                    + "nombre=?, cedula=?, direccion=?, correo=?,"
                    + "telefono=?, latitud=?, longitud=? where id = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, cl.getUsuario());
            stmt.setString(2, cl.getContrasenna());
            stmt.setString(3, cl.getTipo());
            stmt.setString(5, cl.getNombre());
            stmt.setString(4, cl.getCedula());
            stmt.setString(6, cl.getDireccion());
            stmt.setString(7, cl.getCorreo());
            stmt.setString(8, cl.getTelefono());
            stmt.setString(9, cl.getLatitud());
            stmt.setString(10, cl.getLongitud());
            stmt.setInt(11, cl.getId());
            JOptionPane.showMessageDialog(null,"Cliente actualizado correctamente!");
            return stmt.executeUpdate() > 0;       
        } catch (Exception e) {
            throw new RuntimeException("Dato ya existe!!");
        }

    }
}
