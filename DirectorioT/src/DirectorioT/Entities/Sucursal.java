/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DirectorioT.Entities;

/**
 *
 * @author Daniel
 */
//Clase sucursal
public class Sucursal extends Entidad{
    private int id_sucursal;
    
    public Sucursal() {
    }   
    
    public Sucursal(int id_sucursal, int id, String cedula_juridica, String nombre, String direccion, String correo, String telefono, String latitud, String longitud, String usuario, String contrasenna, String tipo) {
        super(id, cedula_juridica, nombre, direccion, correo, telefono, latitud, longitud, usuario, contrasenna, tipo);
        this.id_sucursal = id_sucursal;
    }

    public int getId_sucursal() {
        return id_sucursal;
    }

    public void setId_sucursal(int id_sucursal) {
        this.id_sucursal = id_sucursal;
    }

    @Override
    public String toString() {
        return "Sucursal{" + "id_sucursal=" + id_sucursal + '}';
    }
    

    
}
