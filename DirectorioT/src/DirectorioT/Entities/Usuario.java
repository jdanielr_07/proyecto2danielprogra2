/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DirectorioT.Entities;

/**
 *
 * @author Daniel Rojas
 */
public class Usuario{   
    protected String usuario;
    protected String contrasenna;
    protected String tipo;

    public Usuario() {
    }  

    public Usuario(String usuario, String contrasenna, String tipo) {
        this.usuario = usuario;
        this.contrasenna = contrasenna;
        this.tipo = tipo;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasenna() {
        return contrasenna;
    }

    public void setContrasenna(String contrasenna) {
        this.contrasenna = contrasenna;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return "Usuario{" + "usuario=" + usuario + ", contrasenna=" + contrasenna + ", tipo=" + tipo + '}';
    }
    

    
    
      
}
