/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DirectorioT.Entities;

import java.util.LinkedList;
import java.util.Date;
/**
 *
 * @author Daniel
 */
//Clase Tramite
public class Tramite extends Entidad {
    private int id;
    private String codigo;
    private String titulo;
    private String descripcion;
    private String requisitos;
    private double precio;
    private Date fecha_creacion;
    private Date fecha_modificacion;
    private boolean estado;
    private LinkedList<Tramite> tramites;


    public Tramite() {
        codigo = "";
        titulo = "";
        descripcion="";
        requisitos ="";    
        tramites = new LinkedList<>();
    }

    public Tramite(int id, String codigo, String titulo, String descripcion, String requisitos, double precio, Date fecha_creacion, Date fecha_modificacion, boolean estado, LinkedList<Tramite> tramites) {
        this.id = id;
        this.codigo = codigo;
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.requisitos = requisitos;
        this.precio = precio;
        this.fecha_creacion = fecha_creacion;
        this.fecha_modificacion = fecha_modificacion;
        this.estado = estado;
        this.tramites = tramites;
    }  

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getRequisitos() {
        return requisitos;
    }

    public void setRequisitos(String requisitos) {
        this.requisitos = requisitos;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public Date getFecha_creacion() {
        return fecha_creacion;
    }

    public void setFecha_creacion(Date fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    public Date getFecha_modificacion() {
        return fecha_modificacion;
    }

    public void setFecha_modificacion(Date fecha_modificacion) {
        this.fecha_modificacion = fecha_modificacion;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public LinkedList<Tramite> getTramites() {
        return tramites;
    }

    public void setTramites(LinkedList<Tramite> tramites) {
        this.tramites = tramites;
    }

    @Override
    public String toString() {
        return "Tramite{" + "id=" + id + ", codigo=" + codigo + ", titulo=" + titulo + ", descripcion=" + descripcion + ", requisitos=" + requisitos + ", precio=" + precio + ", fecha_creacion=" + fecha_creacion + ", fecha_modificacion=" + fecha_modificacion + ", estado=" + estado + ", tramites=" + tramites + '}';
    }
    
    
}
